package com.houserent.action;

import com.alibaba.fastjson.JSON;
import my.framework.web.PrintUtil;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Jane
 * @date 2024-04-08 14:57
 */

public class UserAction {

    public String tologin(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        System.out.println(">>>>>>>>>>>>>>>login");

        return "/login.jsp";  //  转发
    }
    public String dologin(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String name = request.getParameter("name");
        String password = request.getParameter("password");

        System.out.println(">>>>>>>>>>>>>>>login");

        if ("admin".equals(name)){
            if ("123456".equals(password)){
                return "/home.jsp";  //  转发
            }else{
                request.setAttribute("msg","密码错误,请检查");
            }
        }else{
            request.setAttribute("msg","账号不存在,请检查");
        }

        return "/login.jsp";  //  转发
    }

    public String register(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        System.out.println(">>>>>>>>>>>>>>>register");

        return "redirect:/register.jsp";  //redirect 重定向

    }

    public void findById(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String username =request.getParameter("username");
        String password =request.getParameter("password");

        System.out.println(">>>>>>>>>>>>>>>findById");

        List<String> list = new ArrayList<>();
        list.add("张三");
        list.add("李四");
        list.add("王五");
        list.add("赵六");

        PrintUtil.write(JSON.toJSONString(list),response);



    }
}
